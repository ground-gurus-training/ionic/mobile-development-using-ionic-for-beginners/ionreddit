import { Component, OnInit } from '@angular/core';
import { RedditService } from '../services/reddit.service';
import { DetailsComponent } from './details/details.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  items: any;
  category = 'pcmasterrace';
  limit: number;

  constructor(
    private redditService: RedditService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.setDefaults();
    this.getPosts(this.category, this.limit);
  }

  setDefaults() {
    const category = localStorage.getItem('category');
    if (category != null) {
      this.category = category;
    } else {
      this.category = 'pcmasterrace';
    }

    const limit = localStorage.getItem('limit');
    if (limit != null) {
      this.limit = parseInt(limit, 0);
    } else {
      this.limit = 10;
    }
  }

  getPosts(category, limit) {
    this.redditService.getPosts(category, limit).subscribe((res: any) => {
      this.items = res.data.children;
    });
  }

  async viewItem(item) {
    const extras = {
      state: {
        data: {
          item
        }
      }
    };

    await this.router.navigateByUrl('/tabs/tab1/details', extras);
  }

  changeCategory() {
    this.getPosts(this.category, this.limit);
  }
}
