import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent {
  item: any;

  constructor(
    private router: Router
  ) {
  }

  async ionViewDidEnter() {
    if (history.state?.data) {
      this.item = history.state.data.item;
    } else {
      await this.router.navigateByUrl('/');
    }
  }

}
