import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RedditService {
  private baseUrl = 'https://www.reddit.com/r';

  constructor(
    private http: HttpClient
  ) {
  }

  getPosts(category, limit) {
    return this.http.get(`${this.baseUrl}/${category}/top.json?limit=${limit}`);
  }
}
